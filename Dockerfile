FROM golang:alpine as builder

ENV VERSION v0.8.5

RUN apk --no-cache add \
    git \
    gcc \
    musl-dev

WORKDIR /go/src

RUN go get -u github.com/drone/drone-ui/dist
RUN go get -u golang.org/x/net/context
RUN go get -u golang.org/x/net/context/ctxhttp
RUN go get -u github.com/golang/protobuf/proto
RUN go get -u github.com/golang/protobuf/protoc-gen-go
RUN go get -u github.com/drone/drone/cmd/drone-agent

WORKDIR /go/src/github.com/drone/drone/cmd/drone-agent
RUN git checkout $VERSION

RUN go install github.com/drone/drone/cmd/drone-agent

FROM registry.gitlab.com/thallian/docker-alpine-s6:master

COPY --from=builder /go/bin/drone-agent /bin/drone-agent

ENV GODEBUG=netdns=go
ENV XDG_CACHE_HOME /home/drone-agent

RUN addgroup -g 131 docker
RUN addgroup -g 2222 drone-agent
RUN adduser -h /home/drone-agent -S -D -u 2222 -G drone-agent drone-agent
RUN addgroup drone-agent docker

RUN apk --no-cache add \
    ca-certificates

ADD /rootfs /

EXPOSE 3000
